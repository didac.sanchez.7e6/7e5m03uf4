﻿using System;
using System.Collections.Generic;
namespace DidacSanchez7e5M03UF4
{
    internal class Datas
    {
        public interface IOrdenable
        {
            int Comparar(IOrdenable x);
            /* Retorna: 0 si this = x
            <0 si this < x
            >0 si this > x
            */
        };
        class Data : IOrdenable
        {
            /*--- Atrivuts ---*/
            public int Dia { get; set; }
            public int Mes { get; set; }
            public int Any { get; set; }


            /*--- Constructors ---*/
            public Data()
            {
                Dia = 1;
                Mes = 1;
                Any = 1980;
            }
            public Data(int dia, int mes, int any)
            {
                Mes = CheckMes(mes);
                Any = any;
                Dia = CheckDia(dia, mes, any);
            }
            public Data(Data data)
            {
                Dia = data.Dia;
                Mes = data.Mes;
                Any = data.Any;
            }
            /*--- Metodos Publicos ---*/

            public override string ToString()
            {
                return $"{Any}/{Mes}/{Dia}";
            }

            public string CompararString(Data data)
            {
                if (this.Any <= data.Any && this.Mes <= data.Mes && this.Dia < data.Dia) return "Més gran";
                if (this.Dia == data.Dia && this.Any == data.Any && this.Mes == data.Mes) return "Es igual";
                return "Es mes petit";
            }
            public string Sumar(int dies)
            {
                dies = Math.Abs(dies);
                Data data = this;
                do
                {
                    data.Dia = CheckDia(data.Dia + 1, data.Mes, data.Any);
                    if (data.Dia == 1) data.Mes = CheckMes(data.Mes + 1);
                    if (data.Mes == 1 && data.Dia == 1) data.Any++;
                    dies--;
                } while (dies != 0);
                return data.ToString();
            }

            public string CalcularDiferencia(Data data)
            {
                int caunt = 0;
                if (this.MirarElGran(data))
                {
                    while (!this.Equals(data))
                    {
                        this.Dia = CheckDia(this.Dia - 1, this.Mes, this.Any);
                        if (this.Dia == 1) this.Mes = CheckMes(--this.Mes);
                        if (this.Mes == 1 && this.Dia == 1) this.Any--;
                        caunt++;
                    }
                }
                while (!this.Equals(data))
                {

                    this.Dia = CheckDia(this.Dia + 1, this.Mes, this.Any);
                    if (this.Dia == 1) this.Mes = CheckMes(++this.Mes);
                    if (this.Mes == 1 && this.Dia == 1) this.Any++;
                    caunt++;
                }
                return $"{caunt} dies";
            }


            /*--- Metodos Hererados ---*/
            public int Comparar(IOrdenable x)
            {
                Data data = (Data)x;
                if (this.Equals(data)) return 0;
                if (!this.MirarElGran(data)) return -1;
                return 1;
            }

            /*--- Metodos Pribados ---*/
            bool MirarElGran(Data data)
            {
                if (this.Any < data.Any) return false;
                if (this.Mes < data.Mes) return false;
                if (this.Dia < data.Dia) return false;
                return true;
            }
            bool Equals(Data data)
            {
                return (this.Dia == data.Dia && this.Mes == data.Mes && this.Any == data.Any);
            }
            static int CheckMes(int mes)
            {
                if (mes < 0 || mes > 13) return 1;
                return mes;
            }
            static int CheckDia(int dia, int mes, int any)
            {
                if (any % 4 == 0 && any % 100 != 0)
                {
                    switch (mes)
                    {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                        case 10:
                        case 12:
                            if (dia > 0 && dia <= 31) return dia;
                            break;
                        case 4:
                        case 6:
                        case 9:
                        case 11:
                            if (dia > 0 && dia <= 30) return dia;
                            break;
                        case 2:
                            if (dia > 0 && dia <= 29) return dia;
                            break;
                    }
                }
                switch (mes)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        if (dia > 0 && dia <= 31) return dia;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        if (dia > 0 && dia <= 30) return dia;
                        break;
                    case 2:
                        if (dia > 0 && dia <= 28) return dia;
                        break;
                }
                return 1;
            }
        }
        public static List<IOrdenable> Ordenar(List<IOrdenable> obj)
        {
            obj.Sort((x, y) => x.Comparar(y));
            return obj;
        }
        static void Main()
        {
            Data data2 = new Data(10, 12, 2024);
            Data data = new Data(29, 1, 2024);
            Data test = new Data(30, 12, 2005);
            Data test2 = new Data(20, 12, 2500);
            Data copia = new Data(data);
            Console.WriteLine(copia.CompararString(data2));
            Console.WriteLine(data.ToString());
            Console.WriteLine(data.Sumar(30));
            Console.WriteLine(data.CalcularDiferencia(data2));
            Data data3 = new Data(28, 3, 2023);
            List<IOrdenable> array = new List<IOrdenable> { data3, data, data2, copia, test, test2 };
            Ordenar(array);
            array.ForEach(data => Console.WriteLine(data));
        }
    }
}
